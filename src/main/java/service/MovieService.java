package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MovieRepository;
import entitys.Movie;
import entitys.User;

@Service

public class MovieService {
	@Autowired
      private MovieRepository  movieRepository;
	 public List<Movie> queryMovies(){
		return movieRepository.queryMovies();
     }
	 public List<Movie> searchMovies(String name){
			return movieRepository.searchMovie(name);
	     }
	 public void saveMovies(){
			 movieRepository.saveMovies();;
	     }
	public Movie findMovie(int a){
		return movieRepository.findMovie(a);
		
	}
	 public void saveUser(User user){
		 movieRepository.saveUser(user);
     }
public User findUser(String a){
	return movieRepository.findUser(a);
	
}
}
