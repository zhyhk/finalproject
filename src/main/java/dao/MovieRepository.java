package dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import entitys.Actor;
import entitys.Movie;
import entitys.User;

@Repository
public class MovieRepository {
	public MovieRepository(){}
	@Autowired
	private SessionFactory sessionFactory;
	  @Transactional
      public List<Movie> queryMovies(){
    	  Session s=sessionFactory.openSession();
    	  List<Movie> l=s.createQuery("from Movie").list();
    	  return l;
      }
	  @Transactional
      public Movie findMovie(int id){
		  Session s=sessionFactory.openSession();
    	  Query q=s.createQuery("from Movie where id=?");
    	  q.setParameter(0, id);
    	  Movie m=(Movie) q.uniqueResult();
    	
    	  return m;
    	  
      }
	  @Transactional
      public List<Movie> searchMovie(String name){
		  Session s=sessionFactory.openSession();
    	  Query q=s.createQuery("from Movie where name like ?");
    	  q.setParameter(0, "%"+name+"%");
    	  List<Movie> l=q.list();
    	  Query q1=s.createQuery("from Actor where name like ?");
    	  q1.setParameter(0, "%"+name+"%");
    	  List<Actor> l1=q1.list();
    	  for(Actor a:l1){
    		  l.addAll(a.getMovies());     
    		  
    	  }
    	  return l;
    	  
      }
	  @Transactional
      public void saveUser(User user){
		  Session s=sessionFactory.openSession();
    	  s.save(user);
      }
	  @Transactional
      public User findUser(String a){
		  Session s=sessionFactory.openSession();
		  Query q=s.createQuery("from User where email=?");
    	  q.setParameter(0, a);
    	  User m=(User) q.uniqueResult();
    	  return m;
      }
	  @Transactional
      public void saveMovies(){
		  Session s1=sessionFactory.openSession();
		  s1.beginTransaction();
		 Movie m=new Movie("a","a","a","a",9);
		 Actor a=new Actor("a");
		 m.getActors().add(a);
		 s1.save(m);
		s1.getTransaction().commit();;
		s1.close();
			
      }
}
