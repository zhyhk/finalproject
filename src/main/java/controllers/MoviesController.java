package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import entitys.Message;
import entitys.Movie;
import entitys.User;
import service.MovieService;



@Controller

public class MoviesController {
   @Autowired  
	private MovieService movieService;
	@RequestMapping(value="/movies",method=RequestMethod.GET)
	
	public  @ResponseBody List<Movie>  getMovies(@RequestParam(value="searchValue",required=false) String searchValue) {
		if(searchValue!=null){
			System.out.print("aaa!!!!!!!!!");
			return movieService.searchMovies(searchValue);
		}else{
		return movieService.queryMovies();}
   }
    
	@RequestMapping(value="/movies/{id}",method=RequestMethod.GET)
	public  @ResponseBody Movie  getMovies(@PathVariable("id") int id) {
		return movieService.findMovie(id);
   }
	@RequestMapping(value="/user",method=RequestMethod.GET)
	public  @ResponseBody Message saveuser(@RequestParam(value="name",required=false) String name,
			@RequestParam(value="email",required=false) String email,@RequestParam(value="password",required=false) String password
			) {
		if(movieService.findUser(email)==null){
			movieService.saveUser(new User(name,email,password));
			return new Message("welcome");
		}else{
			
			return new Message("sorry user name already used");
		}
   }
	@RequestMapping(value="/user/{email}",method=RequestMethod.GET)
	public  @ResponseBody Message saveuser(@PathVariable("email") String email) {
		if(movieService.findUser(email)==null){
			return new Message("no username");
			
		}else{
			return new Message("welcome");
			
		}
   }
}
